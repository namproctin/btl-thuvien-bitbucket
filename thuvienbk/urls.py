from django.urls import path
from . import views

from .views import HomePageView, AboutPageView, HistoryPageView,HotPageView, OldBookPageView, NewBookPageView, them_sach,cap_nhat_so_luong,cap_nhat_hot, PurchasePageView, AuditPageView
urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('history/', HistoryPageView.as_view(), name='history'),
    path('hot/', HotPageView.as_view(), name='hot'),
    path('old_book/', OldBookPageView.as_view(), name='old_book'),
    path('new_book/', NewBookPageView.as_view(), name='new_book'),
    path('them_sach/', them_sach, name='them_sach'),
    path('cap_nhat_so_luong/', cap_nhat_so_luong, name='cap_nhat_so_luong'),
    path('cap_nhat_hot/', cap_nhat_hot, name='cap_nhat_hot'),
    path('purchase/', PurchasePageView.as_view(), name ='purchase'),
    path('audit/<slug:name>', views.AuditPageView, name ='audit'),

]
