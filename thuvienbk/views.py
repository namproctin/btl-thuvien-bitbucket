from django.views.generic import TemplateView
from django.template import loader
from django.http import HttpResponse
from thuvienbk.models import  TheLoaiSach,Sach

from django.views import generic
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect

class HomePageView(TemplateView):
    template_name = 'pages/home.html'

class AboutPageView(TemplateView):
    template_name = 'pages/about.html'

class HistoryPageView(TemplateView):
    template_name = 'pages/history.html'

def AuditPageView(request, name):
    book_list = Sach.objects.all()
    template = loader.get_template('pages/Audit-verHtml/audit.html')
    context = {
        'product_price': book_list.filter(ten_sach = 'name'),
    }
    return HttpResponse(template.render(context, request))
    

class PurchasePageView(TemplateView):
    template_name = 'pages/auditSmartContract/src/MainPage.js'

class HotPageView(generic.ListView):
    template_name = 'pages/hot.html'
    context_object_name = 'book_list'

    def get_queryset(self):
        return Sach.objects.filter(dang_hot=True).order_by('id')


class OldBookPageView(generic.ListView):
    template_name = 'pages/old_book.html'
    context_object_name = 'book_list'

    def get_queryset(self):
        return Sach.objects.all().order_by('id')

class NewBookPageView(generic.ListView):
    template_name = 'pages/new_book.html'
    context_object_name = 'the_loai'

    def get_queryset(self):
        return TheLoaiSach.objects.all()

def them_sach(request):

    sach = Sach(Sach.objects.latest('id').id+1, ten_sach=request.POST["ten_sach"], tac_gia=request.POST["tac_gia"], nam_xuat_ban=request.POST["nam_xuat_ban"], gia_ban=request.POST["gia_ban"],  the_loai=TheLoaiSach.objects.filter(id=request.POST["the_loai"]).first(), so_luong_ton_kho = request.POST['so_luong'])
    sach.save();

    return HttpResponseRedirect('/old_book')

def cap_nhat_so_luong(request):

    sach=Sach.objects.filter(id=request.POST["sach_id"]).first()
    sach.so_luong_ton_kho = request.POST["sl"]
    sach.save();
    return HttpResponseRedirect('/old_book')


def cap_nhat_hot(request):

    sach=Sach.objects.filter(id=request.POST["sach_id"]).first()
    sach.dang_hot = request.POST["hot"]
    sach.save();
    return HttpResponseRedirect('/old_book')
